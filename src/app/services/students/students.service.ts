import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { shareReplay } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { AddUpdateStudentsResponse, DeleteStudentsResponse, GetStudentsResponse, Student } from './students.response';
const url = 'https://localhost:44368/api/students';
//const url = 'http://localhost:4200/assets/students.json';

export interface IStudentsService {
  getStudents(): Observable<GetStudentsResponse>;
  getStudent(id: number): Observable<GetStudentsResponse>;
  saveStudent(student: Student): Observable<AddUpdateStudentsResponse>;
  deleteStudent(id: number):Observable<DeleteStudentsResponse>;
}

@Injectable({
  providedIn: 'root'
})
export class StudentsService implements IStudentsService {
  constructor(protected http: HttpClient) { }

  getStudents(): Observable<GetStudentsResponse> {
    return this.http.get(url).pipe(
      shareReplay({ bufferSize: 1, refCount: true })
    ) as Observable<GetStudentsResponse>
  }

  getStudent(id: number): Observable<GetStudentsResponse> {
    return this.http.get(url + "/" + id).pipe(
      shareReplay({ bufferSize: 1, refCount: true })
    ) as Observable<GetStudentsResponse>
  }

  saveStudent(student: Student): Observable<AddUpdateStudentsResponse> {
    return this.http.post(url, student).pipe(
      shareReplay({ bufferSize: 1, refCount: true })
    ) as Observable<AddUpdateStudentsResponse>
  }

  deleteStudent(id: number) : Observable<DeleteStudentsResponse> {
    return this.http.delete(url + "?id=" + id).pipe(
      shareReplay({ bufferSize: 1, refCount: true })
    ) as Observable<DeleteStudentsResponse>
  }

}
