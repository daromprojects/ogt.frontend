export interface GenericResponse {
  code: string;
  description: string;
}

export interface GetStudentsResponse {
  students?: Student[];
  code: string;
  description: string;
}

export class Student {
  id: number;
  userName: string;
  firstName: string;
  lastName: string;
  age: number;
  career: string;
}

export interface DeleteStudentsResponse extends GenericResponse {
  success: boolean;
}

export interface AddUpdateStudentsResponse extends GenericResponse {
  id: number;
}
