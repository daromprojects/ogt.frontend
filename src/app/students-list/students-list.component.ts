import { Component, OnInit } from '@angular/core';
import { GetStudentsResponse, Student } from '../services/students/students.response';
import { StudentsService } from '../services/students/students.service';

@Component({
  selector: 'app-students-list',
  templateUrl: './students-list.component.html',
  styleUrls: ['./students-list.component.scss']
})
export class StudentsListComponent implements OnInit {

  constructor(protected service: StudentsService) { }

  students: Array<Student> = [];
  isLoading: Boolean = false;

  ngOnInit(): void {
    this.loadStudents();
  }

  deleteStudent(id: number) {
    this.isLoading = true;
    this.service.deleteStudent(id).subscribe(data => {
      if (data.success) {
        this.loadStudents();
      } else {

      }
      this.isLoading = false;
    })
  }

  loadStudents() {
    this.isLoading = true;
    this.service.getStudents().subscribe(data => {
      this.students = (data as GetStudentsResponse).students;
      this.isLoading = false;
    })
  }

}
