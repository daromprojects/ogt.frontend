import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { StudentsService } from '../services/students/students.service';
import { CommonModule } from '@angular/common';
import { AppRoutingModule } from '../app-routing.module';
import { StudentComponent } from './student.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatCardModule } from '@angular/material/card';
import { MatListModule } from '@angular/material/list';
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatButtonModule } from "@angular/material/button";
import { FlexLayoutModule } from "@angular/flex-layout";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

@NgModule({
  declarations: [
    StudentComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    StudentsService,
    CommonModule,
    AppRoutingModule,
    MatProgressSpinnerModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatListModule,
    FlexLayoutModule,
    MatButtonModule,
    MatToolbarModule
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  providers: [StudentsService],
  bootstrap: [StudentComponent]
})

export class StudentModule { }
