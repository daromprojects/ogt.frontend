import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Student } from '../services/students/students.response';
import { StudentsService } from '../services/students/students.service';


@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.scss']
})
export class StudentComponent {

  idStudent: number;
  isLoading: boolean = false;
  student: Student;

  constructor(protected service: StudentsService,
    private activatedRouted: ActivatedRoute,
    private router: Router) {
    this.idStudent = this.activatedRouted.snapshot.params.id;
    this.getStudent(this.idStudent);
  }

  getStudent(idStudent: number) {
    if (idStudent != 0) {
      this.isLoading = true;
      this.service.getStudent(idStudent).subscribe(data => {
        this.student = data.students[0];
        this.isLoading = false;
      });
    } else {
      this.isLoading = false;
      this.student = new Student();
    }
  }

  saveStudent() {
    this.isLoading = true;
    this.updateValues();
    this.service.saveStudent(this.student).subscribe(data => {
      if (data.id !== 0) {

      } else {

      }
      this.isLoading = false;
    });
  }

  updateValues() {
    var firstName = (document.getElementById('txtFirstName') as HTMLInputElement).value;
    var lastName = (document.getElementById('txtLastName') as HTMLInputElement).value;
    var userName = (document.getElementById('txtUserName') as HTMLInputElement).value;
    var age = parseInt((document.getElementById('txtAge') as HTMLInputElement).value);
    var career = (document.getElementById('txtCareer') as HTMLInputElement).value;
    this.student.age = age;
    this.student.career = career;
    this.student.userName = userName;
    this.student.lastName = lastName;
    this.student.firstName = firstName;
  }

}
